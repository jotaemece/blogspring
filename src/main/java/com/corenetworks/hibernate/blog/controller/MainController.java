package com.corenetworks.hibernate.blog.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.corenetworks.hibernate.blog.dao.UserDao;
import com.corenetworks.hibernate.blog.model.User;
//import com.corenetworks.hibernate.spring.Usuario;
//import com.corenetworks.hibernate.spring.UsuarioDao;



@Controller
public class MainController {
	@Autowired
	private UserDao usuarioDao;
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		modelo.addAttribute("listado", usuarioDao.getAll());
		return "index";
	}
	
	@GetMapping(value="/list")
	@ResponseBody
	public String listado() {
		List<User> lUsuario = new ArrayList<>();
		lUsuario = usuarioDao.getAll();
		return "<head>"
				+ "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">"
				+ "<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\n"
		        + "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>\n"
		        + "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>"
				+ "</head><body><h1 align=center>Listado de Usuarios</h1>"
		        + "<div class=\"container\">"
				+ "<table class=\"table table-bordered\">" 
				+ "<tr>"
				+ "<th>Id</th>\n" 
				+ "<th>DNI</th>\n" 
				+ "<th>Nombre</th>\n"
				+ "</tr>"
				+ lUsuario.toString()
				+ "</table></div></body>";
		
	}
	/*
@RequestMapping(value="/")
	
	public String list(Model model) {
		model.addAttribute("listado", usuarioDao.getAll());
		return "listar";
		
	}*/
}
