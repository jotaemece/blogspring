package com.corenetworks.hibernate.blog.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {
	/*
	 * Definición del DataSource para la Conexión a nuestra
	 * base de datos.
	 * Las propiedades son establecidas desde el fichero
	 * application.properties y asignadas usando el objeto
	 * env.
	 */
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("db.driver"));
		dataSource.setUrl(env.getProperty("db.url"));
		dataSource.setUsername(env.getProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		return dataSource;
	}
	/*
	 * Creamos e inicializamos un EntityManagerFactory de JPA
	 * para que sea utilizado a nivel global en mi aplicacion
	 */
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory =
				new LocalContainerEntityManagerFactoryBean();
		
		
		entityManagerFactory.setDataSource(dataSource);
		
		//Indicamos la ruta donde tiene que buscar las clases con anotaciones 
		entityManagerFactory.setPackagesToScan(env.getProperty("entityManager.packagesToScan"));
		
		//Unimos JPA con Hibernate
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		
		//Propiedades de Hibernate
		Properties additionalProperties = new Properties();
		additionalProperties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
		
		additionalProperties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		
		additionalProperties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
		
		entityManagerFactory.setJpaProperties(additionalProperties);
		
		return entityManagerFactory;
	}
	
	/*
	 * Crear e inicializar un gestor de transacciones
	 */
	@Bean
	public JpaTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		
		transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
		
		return transactionManager;
	}
	
	/*
	 * Creamos un Bean que va a ser un postprocessor que ayuda a relanzar
	 * excepciones específicas de cada plataforma en aquellas clases
	 * que vamos a anotar con @Repository
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTraslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
	
	@Autowired //Autowired  =  Autocableado
	private Environment env;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactory;
	
}
