package com.corenetworks.hibernate.blog.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) //sirve para autoincrementar el Id 
	private long id;
	@Column(name="nombre") //El nombre en mi objeto será NAME en la BD
	private String nombre;
	@Column
	private String ciudad;
	@Column
	private String email;
	@Column
	@ColumnTransformer(write=" MD5(?) ") //Encripta la pass cuando se escribe en la BD. Es una encriptacion que no se puede desencriptar
	private String password;
	@Column
	@CreationTimestamp //Crea la fecha actual automaticamente
	private Date fechaAlta; //quizas haya que importarlo como sql
	
	public User() {
	
	}
	
	
	public User(String nombre, String ciudad, String email, String password) {

		this.nombre = nombre;
		this.ciudad = ciudad;
		this.email = email;
		this.password = password;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	@Override
	public String toString() {
		/*return "User [id=" + id + ", nombre=" + nombre + ", ciudad=" + ciudad + ", email=" + email + ", fechaAlta="
				+ fechaAlta +", password=" + password + "]";
	*/
		return "<tr>\n"
		        + "<td>" + getId() + "</td>\n"
		        + "<td>" + getCiudad() + "</td>\n"
		        + "<td>" + getEmail() + "</td>\n"
		        + "<td>" + getFechaAlta() + "</td>\n"
		        + "<td>" + getPassword() + "</td>\n"
		        + "<td>" + getNombre() + "</td>\n"
		        + "</tr>\n";
	
	}
	
	
	
}
