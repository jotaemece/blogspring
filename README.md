# README #

Curso Java8

### ¿Quién soy? ###
* Soy José Manuel Cáceres, actualmente estoy estudiando un curso de Java en Cloud impartido por CoreNetworks en Sevilla.

### ¿Para qué es este repositorio? ###
* Este repositorio se utilizará para realizar un blog.
* En este blog, se subiran actividades y documentación de interés.

### ¿Como me preparo? ###
* Estoy aprendiendo a utilizar:
* Hibernate conectandonos a una BD y realizando transacciones sobre ella.
* Hibernate con JPA y con Springs
* MVC


### Contribución al código ###
* Se permite:
* Corregir posibles errores
* Realizar criticas constructivas
* Mejorar el código

### ¿Con quien hablas? ###
* Con compañeros de clase, con el profesor y con compañeros del mundo Java.